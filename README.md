## About this repo

This repo contains the python script used to create many of the PAM
assay figures in the paper "Rapid and scalable characterization of
CRISPR technologies using an E. coli cell-free
transcription-translation system"
(https://www.biorxiv.org/content/early/2017/07/28/169441). The Python
script expects a folder containing fastq.gz files. The sequencing
facility will give you a .tar file containing the fastq.gz
files. Extract the tar to a folder, but don't unzip the sequencing
files.

## Using the PAM counting script

- Clone or download this repo.
- Install XCode on your Mac if you haven't already
- Install python Anaconda (google it) from their website.
- Create a virtual environment using the following code

```
conda create -n pam_environment python=2.7 biopython pandas
```

- Use the following commands to count a 5N library

```
source activate pam_environment
cd /path/to/pam-analysis
chmod +x count_pams.py
./count_pams.py /path/to/reads/folder /path/to/reads/folder/counts.csv
```

- Use the following commands to count a 10N library

```
source activate pam_environment
cd /path/to/pam-analysis
chmod +x count_pams.py
./count_pams.py -n 10 /path/to/reads/folder /path/to/reads/folder/counts.csv
```

## Output

The output file will be in CSV format and contain the PAM sequence in
the rows and the sample name in the columns.

The file `example-analysis/2017-10-05-pam-counts.csv` is an example of
what the output should look like.

## Analysis

- I provided example R code in the
  `example-analysis/example-pam-analysis.R` to do some analyses on the
  PAM counts. It's kludgy but hopefully helpful.
- There's some pdfs that are created by `example-pam-analysis.R`