#!/usr/bin/env python

import argparse

parser = argparse.ArgumentParser(
    description="""Count the PAMs in fastq.gz files""")
    
parser.add_argument("indir", type=str,
                    help="Directory where fastq.gz files are")
parser.add_argument("out_file", type=str,
                    help="Filename where the counts should go")
parser.add_argument("-n", "--N", default=5, type=int,
                    help="Number of variable nucleotides in the PAM library. Defaults to 5")
parser.add_argument("-q", "--quality", default=30, type=int,
                    help="Phred score required for each of the nucleotides in the PAM in order to be counted. Defaults to 30.")

args = parser.parse_args()

args.regex = "TTTCGTCTTC(" + "".join(["[TCAG]" for i in range(args.N)]) + ")GTCG"


# Begin program

from Bio import SeqIO
import gzip
import io
import re
import os
import itertools
import collections
import pandas as pd

def qual_p(rec, the_match, cutoff=30, match_group=1):
    """Given a fastq record object and a regular expression match,
    check that all the qual scores are above a particular threshold"""
    start = the_match.start(match_group)
    end = the_match.end(match_group)
    phreds = rec.letter_annotations['phred_quality']
    return all([i > cutoff for i in phreds[start:end]])

def get_filename_dict(directory):
    """Finds .fastq.gz files. Returns a dict mapping the names
    stripped of their extension to the filename"""
    filenames = os.listdir(directory)
    to_process = {}
    for i in filenames:
        m = re.search('(.*)\.fastq\.gz', i)
        if not m:
            continue
        to_process[m.group(1)] = os.path.join(directory,i)
    return to_process

def get_counts(file_path, qual):
    print("Processing %s" % file_path)
    n = 0
    container = collections.defaultdict(lambda : 0)
    with io.TextIOWrapper(io.BufferedReader(gzip.open(file_path, "rb"))) as handle:
        #for record in itertools.islice(SeqIO.parse(handle, "fastq"), 100000): # You can uncomment this if you want to sample
        for record in SeqIO.parse(handle, "fastq"):
            n += 1
            if n % 5000000 == 0:
                print("    %i" % n)
            match = re.search(to_match, str(record.seq))
            if match:
                if qual_p(record, match, cutoff=qual):
                    container[match.group(1)] += 1
                else:
                    container["low_quality"] += 1
            else:
                container["no_match"] += 1
    total_counts = sum(container.values())
    print("Reads processed:   %i" % (total_counts))
    print("Low quality reads: {:.1%}%".format(container["low_quality"]/float(total_counts)))
    print("Unmatched reads:   {:.1%}%".format(container["no_match"]/float(total_counts)))
    return container

to_match = re.compile(args.regex)
directory = args.indir
quality = args.quality


counts = pd.DataFrame({k : get_counts(v, quality) for k, v in get_filename_dict(directory).items()})
counts.to_csv(args.out_file)
